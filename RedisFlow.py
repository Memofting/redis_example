from __future__ import annotations

import asyncio
from dataclasses import dataclass
from llist import sllist, sllistnode

import aioredis
import typing as t

T = t.TypeVar("T")


@dataclass
class RedisStreamMessage:
    stream: str
    message_id: str
    fields: t.OrderedDict[bytes, bytes]


@dataclass
class CounterOf(t.Generic[T]):
    count: int
    value: T


class RedisFlow:
    _forks: t.List[Fork]
    _redis: aioredis.Redis
    _latest_id: str
    _request: asyncio.Future

    _stream: str
    _linked_list: sllist
    _current_forks: int

    class _LLIterator(t.AsyncIterator[CounterOf[RedisStreamMessage]]):
        _node: sllistnode = None
        _source: RedisFlow

        def __init__(self, source: RedisFlow):
            self._source = source

        async def __anext__(self) -> CounterOf[RedisStreamMessage]:
            if self._node is not None and self._node.next is not None:
                self._node = self._node.next
                return t.cast(CounterOf[RedisStreamMessage], self._node.value)

            if self._source._request.done():
                loop = asyncio.get_running_loop()
                self._source._request = loop.create_future()
                loop.create_task(self._source.load_messages())

            await self._source._request
            if self._source._redis.closed:
                raise StopAsyncIteration

            if self._node is None:
                self._node = self._source._linked_list.nodeat(0)
            else:
                self._node = self._node.next
            return t.cast(CounterOf[RedisStreamMessage], self._node.value)

    class Fork:
        _flow_iter: t.AsyncIterator[CounterOf[RedisStreamMessage]]
        _source: RedisFlow
        _latest_id: str

        def __init__(self, flow_iter: t.AsyncIterator[CounterOf[RedisStreamMessage]], source: RedisFlow, latest_id: str) -> None:
            self._active = True
            self._flow_iter = flow_iter
            self._source = source
            self._latest_id = latest_id

        def close(self) -> None:
            self._source._current_forks -= 1
            self._active = False
            self.roll_back()

        def __aiter__(self) -> t.AsyncIterator[RedisStreamMessage]:
            return self

        _prev: CounterOf[RedisStreamMessage] = None

        async def __anext__(self) -> RedisStreamMessage:
            if not self._active:
                raise StopAsyncIteration

            value = await self._flow_iter.__anext__()

            if self._prev is not None:
                self._prev.count += 1
                if self._prev.count == self._source._current_forks:
                    try:
                        self._source._linked_list.popleft()
                    except ValueError:
                        pass
            self._latest_id = value.value.message_id
            self._prev = value
            return value.value

        # TODO: to suspending realisation
        def roll_back(self) -> int:
            '''
            iterate from head of the linked list to current browsed node and decrease its view counter
            :return: int - count of decreased nodes
            '''
            self._source._linked_list.popleft()
            count = 0
            if self._latest_id == '$':
                return count
            for head in self._source._linked_list:
                if head.value.message_id == self._latest_id:
                    break
                head.count -= 1
                count += 1
            return count

    def __init__(self, *, redis: aioredis.Redis, stream: str, latest_id: str):
        self._forks = []
        self._redis = redis
        self._stream = stream
        self._linked_list = sllist()
        self._latest_id = latest_id

        loop = asyncio.get_event_loop()
        self._request = loop.create_future()
        self._request.set_result(None)

        self._current_forks = 0

    def __aiter__(self) -> Fork:
        self._current_forks += 1
        return self.Fork(self._LLIterator(self), self, self._latest_id)

    _running: bool = False
    _load_task: asyncio.Task

    async def load_messages(self) -> None:
        try:
            puts = await self._redis.xread(streams=[self._stream], latest_ids=[self._latest_id])
        except (aioredis.ConnectionClosedError, aioredis.ConnectionForcedCloseError):
            return
        for counter in map(lambda p: CounterOf(0, RedisStreamMessage(*p)), puts):
            self._linked_list.append(counter)
            self._latest_id = counter.value.message_id
        self._request.set_result(None)

    async def close(self) -> None:
        self._redis.close()
        if not self._request.done():
            self._request.set_result(None)

        self._forks = []
        for fork in self._forks:
            fork.close()
        await self._redis.wait_closed()

    def __len__(self):
        return len(self._linked_list)
