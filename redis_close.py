import aioredis
import asyncio


async def block_read(redis: aioredis.Redis, stream: str):
    try:
        res = await redis.xread(streams=[stream], latest_ids=['0'])
        print('Get res', res)
    except Exception as e:
        print('Get exc', e)
        raise


async def main():
    redis = await aioredis.create_redis_pool('redis://localhost')
    asyncio.get_running_loop().create_task(block_read(redis, 'lol'))
    redis.close()
    await redis.wait_closed()


if __name__ == '__main__':
    asyncio.run(main())
