import typing as t
import asyncio

import aioredis

from RedisFlow import RedisFlow


async def main():
    stream = 'chan:1'
    redis_ip = 'localhost'

    redis = await aioredis.create_redis_pool(f'redis://{redis_ip}')
    wredis = await aioredis.create_redis_pool(f'redis://{redis_ip}')
    flow = RedisFlow(redis=redis, stream=stream)

    async def reader(sleep_time: float):
        async for value in flow:
            await asyncio.sleep(sleep_time)
            print(f'{sleep_time}: {value}')

    async def writer(sleep_time: float, stream: str, fields: t.Dict[bytes, bytes]):
        for i in range(10):
            await asyncio.sleep(sleep_time)
            fields['time'] = i
            await wredis.xadd(stream, fields)

    async def len_logger(sleep_time: float):
        while True:
            await asyncio.sleep(sleep_time)
            print(f'length: {len(flow)}')

    rd1 = reader(0.11)
    rd2 = reader(0.12)
    rd3 = reader(0.13)

    wr1 = writer(5, 'chan:1', {b'chan:1': b'hello 1'})
    wr2 = writer(6, 'chan:1', {b'chan:1': b'hello 2'})
    wr3 = writer(7, 'chan:1', {b'chan:1': b'hello 3'})

    llog = len_logger(0.5)

    result = await asyncio.gather(rd1,
                                  rd2, rd3,
                                  wr1,
                                  wr2, wr3,
                                  llog,
                                  return_exceptions=True)
    print(result)


if __name__ == '__main__':
    asyncio.run(main())
