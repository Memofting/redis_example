import typing as t

import asyncio
import aioredis

import pytest

from RedisFlow import RedisFlow

count = 0


@pytest.fixture
def stream() -> str:
    global count
    count += 1
    return f'test_name_{count}'


@pytest.fixture
async def flow(stream: str) -> RedisFlow:
    redis_ip = 'localhost'
    redis = await aioredis.create_redis_pool(f'redis://{redis_ip}')
    _flow = RedisFlow(redis=redis, stream=stream, latest_id='0')
    yield _flow
    await _flow.close()


@pytest.fixture
async def writer_flow_pair(flow: RedisFlow) -> t.Tuple[aioredis.Redis, RedisFlow]:
    redis_ip = 'localhost'
    redis = await aioredis.create_redis_pool(f'redis://{redis_ip}')
    yield redis, flow
    await redis.xtrim(flow._stream, 0, exact_len=True)
    await redis.xtrim(flow._stream, 1)
    redis.close()
    await redis.wait_closed()


@pytest.mark.asyncio
async def test_message_ordering(writer_flow_pair: t.Tuple[aioredis.Redis, RedisFlow]) -> None:
    redis_writer = writer_flow_pair[0]
    flow = writer_flow_pair[1]

    readed = []
    writed = [{str(x).encode(): str(x).encode()} for x in range(10)]

    async def reader():
        async for value in flow:
            readed.append(value.fields)

    async def writer():
        for fields in writed:
            await redis_writer.xadd(flow._stream, fields)
        await asyncio.sleep(0.1)  # our guaranties that all messages will read

    asyncio.create_task(reader())
    await writer()

    assert readed == writed


@pytest.mark.asyncio
async def test_multiple_connections(writer_flow_pair: t.Tuple[aioredis.Redis, RedisFlow]) -> None:
    redis_writer = writer_flow_pair[0]
    flow = writer_flow_pair[1]

    from collections import Counter

    readed = []
    writed = [(str(x).encode(), str(x).encode()) for x in range(10)]

    async def reader():
        async for value in flow:
            readed.extend(zip(value.fields.keys(), value.fields.values()))

    async def writer():
        for field in writed:
            await redis_writer.xadd(flow._stream, {field[0]: field[1]})
        await asyncio.sleep(1)  # our guaranties that all messages will read

    asyncio.create_task(reader())
    asyncio.create_task(reader())
    asyncio.create_task(reader())
    await writer()

    assert len(readed) == len(writed) * 3
    counter = Counter(readed)
    for key, value in counter.most_common():
        assert value == 3
        assert key in writed


@pytest.mark.asyncio
async def test_close_fork_connections(writer_flow_pair: t.Tuple[aioredis.Redis, RedisFlow]) -> None:
    redis_writer = writer_flow_pair[0]
    flow = writer_flow_pair[1]

    readed = []
    writed = [(str(x).encode(), str(x).encode()) for x in range(10)]

    loop = asyncio.get_running_loop()
    request = loop.create_future()

    async def stoppper(fork: RedisFlow.Fork):
        await fork.__anext__()

    async def reader(fork: RedisFlow.Fork, close_on_end: bool = False):
        for _ in range(10):
            readed.append(await fork.__anext__())
        await request
        if close_on_end:
            fork.close()

    async def writer():
        for field in writed:
            await redis_writer.xadd(flow._stream, {field[0]: field[1]})
        await asyncio.sleep(1)  # our guaranties that all messages will read

    fork1 = flow.__aiter__()
    fork2 = flow.__aiter__()
    fork3 = flow.__aiter__()
    treader1 = asyncio.create_task(reader(fork1, False))
    treader2 = asyncio.create_task(reader(fork2, True))
    asyncio.create_task(stoppper(fork3))
    await writer()

    assert len(readed) == len(writed) * 2
    for value in list(flow._linked_list)[:-1]:
        assert value.count == 2

    request.set_result(None)
    await treader1
    await treader2

    for value in list(flow._linked_list)[:-1]:
        assert value.count == 1
